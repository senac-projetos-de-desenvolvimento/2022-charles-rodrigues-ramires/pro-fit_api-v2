-- CreateTable
CREATE TABLE "treinos" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "nomeTreino" TEXT NOT NULL,
    "dificuldade" TEXT NOT NULL,
    "avaliacao" INTEGER,
    "qTreinou" BOOLEAN,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" DATETIME NOT NULL
);

-- CreateTable
CREATE TABLE "exercicios" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "nomeExercicio" TEXT NOT NULL,
    "serie" INTEGER NOT NULL,
    "descanso" TEXT NOT NULL,
    "repeticao" INTEGER NOT NULL,
    "foto" TEXT,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" DATETIME NOT NULL
);

-- CreateTable
CREATE TABLE "treinos_has_exercicios" (
    "treinoId" TEXT NOT NULL,
    "exercicioId" TEXT NOT NULL,

    PRIMARY KEY ("treinoId", "exercicioId"),
    CONSTRAINT "treinos_has_exercicios_treinoId_fkey" FOREIGN KEY ("treinoId") REFERENCES "treinos" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "treinos_has_exercicios_exercicioId_fkey" FOREIGN KEY ("exercicioId") REFERENCES "exercicios" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "treinos_has_usuarios" (
    "treinoId" TEXT NOT NULL,
    "usuarioId" TEXT NOT NULL,

    PRIMARY KEY ("treinoId", "usuarioId"),
    CONSTRAINT "treinos_has_usuarios_treinoId_fkey" FOREIGN KEY ("treinoId") REFERENCES "treinos" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "treinos_has_usuarios_usuarioId_fkey" FOREIGN KEY ("usuarioId") REFERENCES "usuarios" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
