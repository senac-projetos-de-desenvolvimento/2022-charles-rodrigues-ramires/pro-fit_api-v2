import { prisma } from "../../prisma/client";
import { AppError } from "../../errors/AppError";
import { CreateTreinoHasExercicioDTO } from "../dtos/CreateTreinoHasExercicioDTO";

export class CreateTreinoHasExercicioCase {
  async execute({
    treinoId,
    exercicioId,
  }: CreateTreinoHasExercicioDTO): Promise<void> {
    const treinoExiste = await prisma.treino.findUnique({
      where: {
        id: treinoId,
      },
    });

    if (!treinoExiste) {
      throw new AppError("Treino não existe", 400);
    }

    const exercicioExiste = await prisma.exercicio.findUnique({
      where: {
        id: exercicioId,
      },
    });

    if (!exercicioExiste) {
      throw new AppError("Exercicio não existe", 400);
    }

    await prisma.treino_has_Exercicio.create({
      data: {
        treinoId,
        exercicioId,
      },
    });
  }
}

export class CreateListTreinoHasExerciciosCase {
  async execute() {
    const treinosExercicios = await prisma.treino.findMany({
      include: {
        treinos_has_exercicios: {
          select: {
            exercicio: {
              select: {
                nomeExercicio: true,
                serie: true,
                descanso: true,
                repeticao: true,
                foto: true,
              },
            },
          },
        },
      },
    });
    return treinosExercicios;
  }
}

export class CreateListTreinoHasExerciciosIdCase {
  async execute(treinoId: string) {
    const treinoExercicio = await prisma.treino_has_Exercicio.findMany({
      where: {
        treinoId,
      },
      select: {
        exercicio: {
          select: {
            id: true,
            nomeExercicio: true,
            serie: true,
            descanso: true,
            repeticao: true,
            foto: true,
          },
        },
      },
    });
    return treinoExercicio;
  }
}
