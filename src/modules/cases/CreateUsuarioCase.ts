import { prisma } from "../../prisma/client";
import { CreateUsuarioDTO } from "../dtos/CreateUsuarioDTO";
import { Usuario } from "@prisma/client";
import { AppError } from "../../errors/AppError";
import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";

export class CreateUsuarioCase {
  async execute({
    nome,
    email,
    senha,
    funcao,
  }: CreateUsuarioDTO): Promise<Usuario> {
    const usuarioExiste = await prisma.usuario.findUnique({
      where: {
        email,
      },
    });

    if (usuarioExiste) {
      throw new AppError("Esse usuário já existe", 400);
    }

    const usuario = await prisma.usuario.create({
      data: {
        nome,
        email,
        senha,
        funcao,
      },
    });

    return usuario;
  }
}

export class CreateListUsuarioCase {
  async execute() {
    const usuarios = await prisma.usuario.findMany({});
    return usuarios;
  }
}

export class DeleteUsuarioCase {
  async execute(id: string) {
    await prisma.usuario.delete({
      where: {
        id,
      },
    });
  }
}

export class CreateLoginUsuarioCase {
  async execute(email: string, senha: string) {
    if (!email || !senha) {
      throw new AppError("Email ou senha incorretos.", 400);
    }
    const login = await prisma.usuario.findUnique({
      where: {
        email,
      },
    });
    if (!login) {
      throw new AppError("Email ou senha incorretos.", 400);
    }

    if (await bcrypt.compare(senha, login.senha)) {
      const acessToken = await jwt.sign(login, process.env.JWT_KEY ?? "", {
        expiresIn: "8h",
      });

      const { senha: _, ...usuario } = login;

      return { ...usuario, acessToken };
    } else {
      throw new AppError("Email ou senha incorretos.", 400);
    }
  }
}

export class UpdateUsuario {
  async execute(id: string, email: string) {
    await prisma.usuario.update({
      where: {
        id,
      },
      data: {
        email,
      },
    });
  }
}
