import { AppError } from "../../errors/AppError";
import { prisma } from "../../prisma/client";
import { CreateTreinoHasUsuarioDTO } from "../dtos/CreateTreinoHasUsuarioDTO";

export class CreateTreinoHasUsuarioCase {
  async execute({
    treinoId,
    usuarioId,
  }: CreateTreinoHasUsuarioDTO): Promise<void> {
    const treinoExiste = await prisma.treino.findUnique({
      where: {
        id: treinoId,
      },
    });

    if (!treinoExiste) {
      throw new AppError("Treino não existe", 400);
    }

    const usuarioExiste = await prisma.usuario.findUnique({
      where: {
        id: usuarioId,
      },
    });

    if (!usuarioExiste) {
      throw new AppError("Usuário não existe.", 400);
    }

    await prisma.treino_has_Usuario.create({
      data: {
        treinoId,
        usuarioId,
      },
    });
  }
}
