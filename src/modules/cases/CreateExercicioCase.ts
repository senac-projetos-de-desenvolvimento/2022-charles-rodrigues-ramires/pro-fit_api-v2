import { prisma } from "../../prisma/client";
import { Exercicio } from "@prisma/client";
import { CreateExercicioDTO } from "../../modules/dtos/CreateExercicioDTO";

export class CreateExercicioCase {
  async execute({
    nomeExercicio,
    serie,
    descanso,
    repeticao,
    foto,
  }: CreateExercicioDTO): Promise<Exercicio> {
    const exercicio = await prisma.exercicio.create({
      data: {
        nomeExercicio,
        serie,
        descanso,
        repeticao,
        foto,
      },
    });
    return exercicio;
  }
}

export class CreateListExercicioCase {
  async execute() {
    const exercicios = await prisma.exercicio.findMany({});
    return exercicios;
  }
}

export class UpdateExercicioFoto {
  async execute(id: string, foto: string) {
    await prisma.exercicio.update({
      where: {
        id,
      },
      data: {
        foto,
      },
    });
  }
}
