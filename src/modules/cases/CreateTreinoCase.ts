import { prisma } from "../../prisma/client";
import { CreateTreinoDTO } from "../dtos/CreateTreinoDTO";
import { Treino } from "@prisma/client";
import { CreateTreinoHasExercicioCase } from "./CreateTreinoHasExercicioCase";
export class CreateTreinoCase {
  async execute({
    nomeTreino,
    dificuldade,
    avaliacao,
    qTreinou,
    listExercicios,
  }: CreateTreinoDTO): Promise<Treino> {
    const treino = await prisma.treino.create({
      data: {
        nomeTreino,
        dificuldade,
        avaliacao,
        qTreinou,
      },
    });

    listExercicios.forEach((exercicioId) =>
      new CreateTreinoHasExercicioCase().execute({
        treinoId: treino.id,
        exercicioId,
      })
    );

    return treino;
  }
}

export class CreateListTreinoCase {
  async execute() {
    const treinos = await prisma.treino.findMany({});
    return treinos;
  }
}

export class DeleteTreinoCase {
  async execute(id: string) {
    await prisma.treino.delete({
      where: {
        id,
      },
    });
  }
}

export class UpdateTreinoCase {
  async execute(id: string, nomeTreino: string, dificuldade: string) {
    await prisma.treino.update({
      where: {
        id,
      },
      data: {
        nomeTreino,
        dificuldade,
      },
    });
  }
}

export class UpdateAvaliacaoCase {
  async execute(id: string, avaliacao: number) {
    await prisma.treino.update({
      where: {
        id,
      },
      data: {
        avaliacao,
      },
    });
  }
}
