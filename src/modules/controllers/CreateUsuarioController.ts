import { Request, Response } from "express";
import { usuarioRoutes } from "../../routes/usuario.routes";
import {
  CreateListUsuarioCase,
  CreateLoginUsuarioCase,
  CreateUsuarioCase,
  DeleteUsuarioCase,
  UpdateUsuario,
} from "../cases/CreateUsuarioCase";
import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
import { AppError } from "../../errors/AppError";

const randomSalt = Math.random() * 10;

export class CreateUsuarioController {
  async handle(req: Request, res: Response) {
    const { nome, email, senha, funcao } = req.body;

    const hash = bcrypt.hashSync(senha, randomSalt);

    const createUsuario = new CreateUsuarioCase();
    const result = await createUsuario.execute({
      nome,
      email,
      senha: hash,
      funcao,
    });

    return res.status(201).json(result);
  }

  async handleList(req: Request, res: Response) {
    const listUsuarios = new CreateListUsuarioCase();

    const result = await listUsuarios.execute();

    return res.status(201).json(result);
  }

  async handleDelete(req: Request, res: Response) {
    const { id } = req.params;
    const deleteUsuario = new DeleteUsuarioCase();
    const result = await deleteUsuario.execute(id);

    return res.status(201).send("Usuário deletado com sucesso.");
  }

  async handleLogin(req: Request, res: Response) {
    const { email, senha } = req.body;
    const login = new CreateLoginUsuarioCase();
    const result = await login.execute(email, senha);
    return res.status(201).json(result);
  }

  async handleUpdateUsuario(req: Request, res: Response) {
    const { id } = req.params;
    const { email } = req.body;

    if (email) {
      throw new AppError("Esse email já foi cadastrado.", 400);
    }

    const updateUsuario = new UpdateUsuario();
    const result = await updateUsuario.execute(id, email);

    return res.status(201).json(result);
  }
}
