import { Request, Response } from "express";
import { CreateTreinoHasUsuarioCase } from "../cases/CreateTreinoHasUsuarioCase";

export class CreateTreinoHasUsuarioController {
  async handle(req: Request, res: Response) {
    const { treinoId, usuarioId } = req.body;

    const createTreinoHasUsuarioCase = new CreateTreinoHasUsuarioCase();
    const result = await createTreinoHasUsuarioCase.execute({
      treinoId,
      usuarioId,
    });
    return res.status(201).send();
  }
}
