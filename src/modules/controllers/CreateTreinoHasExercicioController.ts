import { Request, Response } from "express";
import {
  CreateListTreinoHasExerciciosCase,
  CreateListTreinoHasExerciciosIdCase,
  CreateTreinoHasExercicioCase,
} from "../cases/CreateTreinoHasExercicioCase";

export class CreateTreinoHasExercicioController {
  async handle(req: Request, res: Response) {
    const { treinoId, exercicioId } = req.body;

    const createTreinoHasExercicioCase = new CreateTreinoHasExercicioCase();
    const result = await createTreinoHasExercicioCase.execute({
      treinoId,
      exercicioId,
    });

    return res.status(201).send();
  }

  async hadleList(req: Request, res: Response) {
    const treinoExercicios = new CreateListTreinoHasExerciciosCase();
    const result = await treinoExercicios.execute();

    return res.status(201).json(result);
  }

  async handleListID(req: Request, res: Response) {
    const { id } = req.params;
    const listTreinoHasExercicioId = new CreateListTreinoHasExerciciosIdCase();
    const result = await listTreinoHasExercicioId.execute(id);
    return res.status(201).json(result);
  }
}
