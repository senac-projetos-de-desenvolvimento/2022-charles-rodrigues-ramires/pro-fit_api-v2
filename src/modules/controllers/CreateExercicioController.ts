import { Request, Response } from "express";
import {
  CreateExercicioCase,
  CreateListExercicioCase,
  UpdateExercicioFoto,
} from "../cases/CreateExercicioCase";

export class CreateExercicioController {
  async handle(req: Request, res: Response) {
    const { nomeExercicio, serie, descanso, repeticao, foto } = req.body;

    const createExercicio = new CreateExercicioCase();
    const result = await createExercicio.execute({
      nomeExercicio,
      serie,
      descanso,
      repeticao,
      foto,
    });

    return res.status(201).json(result);
  }

  async handleList(req: Request, res: Response) {
    const listExercicios = new CreateListExercicioCase();

    const result = await listExercicios.execute();

    return res.status(201).json(result);
  }

  async handleUpadateFoto(req: Request, res: Response) {
    const { id } = req.params;
    const { foto } = req.body;
    const uptadeExercicio = new UpdateExercicioFoto();
    const result = await uptadeExercicio.execute(id, foto);
    return res.status(201).json(result);
  }
}
