import { prisma } from "@prisma/client";
import { Request, Response } from "express";
import { AppError } from "../../errors/AppError";
import {
  CreateListTreinoCase,
  CreateTreinoCase,
  DeleteTreinoCase,
  UpdateAvaliacaoCase,
  UpdateTreinoCase,
} from "../cases/CreateTreinoCase";

export class CreateTreinoController {
  async handle(req: Request, res: Response) {
    const { nomeTreino, dificuldade, avaliacao, qTreinou, listExercicios } =
      req.body;

    const createTreino = new CreateTreinoCase();
    const result = await createTreino.execute({
      nomeTreino,
      dificuldade,
      avaliacao,
      qTreinou,
      listExercicios,
    });

    return res.status(201).json(result);
  }

  async handleList(req: Request, res: Response) {
    const listTreino = new CreateListTreinoCase();

    const result = await listTreino.execute();

    return res.status(201).json(result);
  }

  async handleDelete(req: Request, res: Response) {
    const { id } = req.params;
    const deleteTreino = new DeleteTreinoCase();
    const result = await deleteTreino.execute(id);

    return res.status(201).send("Treino deletado com sucesso.");
  }

  async handleUpdate(req: Request, res: Response) {
    const { id } = req.params;
    const { nomeTreino, dificuldade } = req.body;

    if (!nomeTreino || !dificuldade) {
      throw new AppError("Erro ao editar.", 400);
    }

    const updateTreino = new UpdateTreinoCase();
    const result = await updateTreino.execute(id, nomeTreino, dificuldade);
    return res.status(201).json(result);
  }

  async handleUpdateAvaliacao(req: Request, res: Response) {
    const { id } = req.params;
    const { avaliacao } = req.body;

    if (avaliacao < 0 || avaliacao > 5) {
      throw new AppError("A avaliacão tem que estar entre 1 e 5.", 400);
    }

    const updateAvaliacao = new UpdateAvaliacaoCase();
    const result = await updateAvaliacao.execute(id, avaliacao);

    return res.status(201).json(result);
  }
}
