export interface CreateUsuarioDTO {
  nome: string;
  email: string;
  senha: string;
  funcao: string;
}
