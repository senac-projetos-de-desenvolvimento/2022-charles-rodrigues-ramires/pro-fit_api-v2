export interface CreateTreinoDTO {
  nomeTreino: string;
  dificuldade: string;
  avaliacao: number;
  qTreinou: boolean;
  listExercicios: string[];
}
