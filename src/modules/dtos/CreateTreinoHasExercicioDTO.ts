export interface CreateTreinoHasExercicioDTO {
  treinoId: string;
  exercicioId: string;
}
