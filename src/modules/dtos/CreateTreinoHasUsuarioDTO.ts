export interface CreateTreinoHasUsuarioDTO {
  treinoId: string;
  usuarioId: string;
}
