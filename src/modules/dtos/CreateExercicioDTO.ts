export interface CreateExercicioDTO {
  nomeExercicio: string;
  serie: number;
  descanso: string;
  repeticao: number;
  foto: string;
}
