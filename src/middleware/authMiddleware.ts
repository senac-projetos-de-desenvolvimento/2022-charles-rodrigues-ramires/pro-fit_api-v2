import { NextFunction, Request, Response } from "express";
import { AppError } from "../errors/AppError";
import jwt from "jsonwebtoken";
import { Usuario } from "@prisma/client";
import { prisma } from "../prisma/client";

type JwtPayload = {
  id: string;
};

export const authMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { authorization } = req.headers;

  if (!authorization) {
    throw new AppError("Não autorizado", 401);
  }

  const token = authorization.split(" ")[1];
  const { id } = jwt.verify(token, process.env.JWT_KEY ?? "") as JwtPayload;

  const user = await prisma.usuario.findFirst({ where: { id } });

  if (!user) {
    throw new AppError("Não autorizado.", 401);
  }

  const { senha: _, ...logged } = user;
  req.user = logged;
  next();
};
