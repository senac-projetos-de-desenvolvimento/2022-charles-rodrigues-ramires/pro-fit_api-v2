import { Router } from "express";
import { CreateTreinoHasExercicioController } from "../modules/controllers/CreateTreinoHasExercicioController";

const treinoHasExercicioController = new CreateTreinoHasExercicioController();
const treinoHasExercicioRoutes = Router();

treinoHasExercicioRoutes
  .post("/", treinoHasExercicioController.handle)
  .get("/", treinoHasExercicioController.hadleList)
  .get("/:id", treinoHasExercicioController.handleListID);

export { treinoHasExercicioRoutes };
