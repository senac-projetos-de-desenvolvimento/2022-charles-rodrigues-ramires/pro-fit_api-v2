import { Router } from "express";
import { routes } from ".";
import { authMiddleware } from "../middleware/authMiddleware";
import { CreateTreinoController } from "../modules/controllers/CreateTreinoController";

const treinoController = new CreateTreinoController();
const treinoRoutes = Router();

treinoRoutes
  .post("/", treinoController.handle)
  .get("/", treinoController.handleList)
  .delete("/:id", treinoController.handleDelete)
  .patch("/:id", treinoController.handleUpdate)
  .patch("/avaliacao/:id", treinoController.handleUpdateAvaliacao);

export { treinoRoutes };
