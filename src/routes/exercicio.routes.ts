import { Router } from "express";
import { CreateExercicioController } from "../modules/controllers/CreateExercicioController";

const exercicioController = new CreateExercicioController();
const exercicioRoutes = Router();

exercicioRoutes
  .post("/", exercicioController.handle)
  .get("/", exercicioController.handleList)
  .patch("/:id", exercicioController.handleUpadateFoto);

export { exercicioRoutes };
