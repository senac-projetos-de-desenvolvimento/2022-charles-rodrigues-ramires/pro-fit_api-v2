import { Router } from "express";
import { CreateUsuarioController } from "../modules/controllers/CreateUsuarioController";

const usuarioController = new CreateUsuarioController();
const usuarioRoutes = Router();

usuarioRoutes
  .post("/", usuarioController.handle)
  .get("/", usuarioController.handleList)
  .delete("/:id", usuarioController.handleDelete)
  .post("/login", usuarioController.handleLogin)
  .patch("/:id", usuarioController.handleUpdateUsuario);

export { usuarioRoutes };
