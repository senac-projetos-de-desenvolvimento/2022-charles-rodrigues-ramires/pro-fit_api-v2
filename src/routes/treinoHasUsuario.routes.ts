import { Router } from "express";
import { CreateTreinoHasUsuarioController } from "../modules/controllers/CreateTreinoHasUsuarioController";

const treinoHasUsuarioController = new CreateTreinoHasUsuarioController();
const treinoHasUsuarioRoutes = Router();

treinoHasUsuarioRoutes.post("/", treinoHasUsuarioController.handle);

export { treinoHasUsuarioRoutes };
