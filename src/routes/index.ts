import { Router } from "express";
import { exercicioRoutes } from "./exercicio.routes";
import { treinoRoutes } from "./treino.routes";
import { treinoHasExercicioRoutes } from "./treinoHasExercicio.routes";
import { treinoHasUsuarioRoutes } from "./treinoHasUsuario.routes";
import { usuarioRoutes } from "./usuario.routes";

const routes = Router();

routes.use("/usuario", usuarioRoutes);
routes.use("/treinos", treinoRoutes);
routes.use("/exercicios", exercicioRoutes);
routes.use("/treino_has_exercicio", treinoHasExercicioRoutes);
routes.use("/treino_has_usuario", treinoHasUsuarioRoutes);

export { routes };
